import { Component, OnInit, NgZone } from "@angular/core";
let ipcRenderer = require("electron").ipcRenderer;
import { Router } from "@angular/router";
import { DogsService } from "../get-test/services/dogs.service";

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.scss"],
  providers: [DogsService]
})
export class HomeComponent implements OnInit {
  public breeds: any = null;
  public fetch: boolean = true;
  constructor(
    private ngZone: NgZone,
    private _router: Router,
    private _dogsService: DogsService
  ) {}

  async ngOnInit() {
    await this.allBreeds();
    console.log(this.breeds);
    ipcRenderer.on("goto-sar", (event, arg) => {
      this.ngZone.run(() => {
        this.openCheck();
      });
    });
  }
  openCheck() {
    this._router.navigate(["/home"]);
  }
  async allBreeds() {
    const data = new Promise((resolve, reject) => {
      try {
        this._dogsService.getAllBreeds().subscribe(data => {
          this.fetch = false;
          resolve(data);
        });
      } catch (error) {
        reject(error);
      }
    });
    this.breeds = Object.keys((await data)["message"]);
  }
}
