import { Component, OnInit, NgZone } from "@angular/core";
// let ipcRenderer = require("electron").ipcRenderer;
import { DogsService } from "./services/dogs.service";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-get",
  templateUrl: "./get.component.html",
  styleUrls: ["./get.component.scss"],
  providers: [DogsService]
})
export class GetComponent implements OnInit {
  public breedImage: any = null;
  public name: string;
  public fetch: boolean = true;
  constructor(
    private ngZone: NgZone,
    private _dogsService: DogsService,
    private route: ActivatedRoute
  ) {}

  async ngOnInit() {
    this.getAllImagesForParams(await this.searchForParam());
  }
  async getAllImagesForParams(name) {
    try {
      let promise = new Promise((resolve, reject) => {
        try {
          this._dogsService.getAllBreedImages(name).subscribe(data => {
            this.fetch = false;
            resolve(data);
           
          });
        } catch (error) {
          reject(error);
        }
      });
      this.breedImage = (await promise)["message"];
      console.log(this.breedImage);
    } catch (error) {
      console.log(error);
    }
  }

  async searchForParam() {
    return new Promise((resolve, reject) => {
      try {
        this.route.paramMap.subscribe(routeParam => {
          this.name = routeParam["params"]["name"];
          resolve(routeParam["params"]["name"]);
        });
      } catch (error) {
        reject(error);
      }
    });
  }
}
