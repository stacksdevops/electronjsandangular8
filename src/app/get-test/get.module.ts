import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { GetRoutingModule } from "./get-routing.module";

import { GetComponent } from "./get.component";
import { SharedModule } from "../shared/shared.module"; 
@NgModule({
  declarations: [GetComponent],
  imports: [CommonModule, SharedModule, GetRoutingModule]
})
export class GetModule {}
