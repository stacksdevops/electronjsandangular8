import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { IDogs } from "../Interfaces/dogs";
import { Observable } from "rxjs";
 
@Injectable()
export class DogsService {
  //private _url: string = "https://dog.ceo/api/breeds/list/all";
  constructor(private http: HttpClient) {}

  getAllBreeds(): Observable<IDogs> {
    return this.http.get<IDogs>('https://dog.ceo/api/breeds/list/all');
  }
  getAllBreedImages(name:string): Observable<IDogs> {
    return this.http.get<IDogs>(`https://dog.ceo/api/breed/${name}/images`);
  }
}
